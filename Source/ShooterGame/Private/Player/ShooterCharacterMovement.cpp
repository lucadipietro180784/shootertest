// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Player/ShooterCharacterMovement.h"

//----------------------------------------------------------------------//
// UPawnMovementComponent
//----------------------------------------------------------------------//
UShooterCharacterMovement::UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	JetpackEnergy = 0;
}


float UShooterCharacterMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const AShooterCharacter* ShooterCharacterOwner = Cast<AShooterCharacter>(PawnOwner);
	if (ShooterCharacterOwner)
	{
		if (ShooterCharacterOwner->IsTargeting())
		{
			MaxSpeed *= ShooterCharacterOwner->GetTargetingSpeedModifier();
		}
		if (ShooterCharacterOwner->IsRunning())
		{
			MaxSpeed *= ShooterCharacterOwner->GetRunningSpeedModifier();
		}
	}

	return MaxSpeed;
}

void  UShooterCharacterMovement::PerformMovement(float DeltaTime)
{
	Super::PerformMovement(DeltaTime);

	AShooterCharacter* Character = Cast<AShooterCharacter>(PawnOwner);
	if (Character)
	{
		if (Character->bIsUsingJetpack)
		{
			JetpackEnergy -= Character->JetpackConsumeRate * DeltaTime;
			if (JetpackEnergy < 0)
			{
				JetpackEnergy = 0;
				Character->StopJetpack();
				Character->DisableJetpack();
			}
			Velocity.Z += Character->JetpackForce * DeltaTime;
		}

		else if (Character->bIsJetpackEnabled)
		{
			JetpackEnergy += DeltaTime * Character->JetpackRefillRate;
			if (JetpackEnergy >= Character->JetpackFullEnergy)
			{
				JetpackEnergy = Character->JetpackFullEnergy;
			}
		}

		if (Character->bIsUsingTeleport)
		{
			FHitResult res;
			float distance = Character->ReturnTeleportDistance();
			SafeMoveUpdatedComponent(GetOwner()->GetActorForwardVector() * distance, GetOwner()->GetActorRotation(), false, res, ETeleportType::TeleportPhysics);
			Character->bIsUsingTeleport = false;
		}
	}


}
