// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapons/ShooterWeapon.h"
#include "WeaponPickup.generated.h"



UCLASS()
class SHOOTERGAME_API AWeaponPickup : public AActor
{
	GENERATED_BODY()


public:
	// Sets default values for this actor's properties
	AWeaponPickup();

	/** check if pawn can use this pickup */
	bool CanBePickedUp(class AShooterCharacter* TestPawn) const;

	/** get currently weapon type */
	UFUNCTION(BlueprintCallable)
	TSubclassOf<AShooterWeapon> GetTypeOfWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** give pickup */
	void GivePickupTo(class AShooterCharacter* Pawn);

	/** handle touches */
	void PickupOnTouch(class AShooterCharacter* Pawn);

	/** pickup on touch */
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	/** Destroy Pickup after DestroyTime */
	void Disappear();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:

	/** sound played when player picks it up */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	USoundCue* PickupSound;

	/** how long it takes to respawn? */
	UPROPERTY(EditDefaultsOnly, Category = Pickup)
	float DestroyTime;

	/* The character who has picked up this pickup */
	UPROPERTY(Transient)
	AShooterCharacter* PickedUpBy;

	/** which weapon gets ammo? */
	UPROPERTY(EditDefaultsOnly, Category = Pickup)
	TSubclassOf<AShooterWeapon> WeaponType;

public:

	/** how much ammo does it give? */
	UPROPERTY(EditDefaultsOnly, Category = Pickup)
	int32 Ammo;

};
